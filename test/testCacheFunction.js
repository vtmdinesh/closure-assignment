const cacheFunction = require("../cacheFunction")


const square = (num) => num * num;

const cachedSquare = cacheFunction(square);

console.log(cachedSquare(2)); // square should be invoked
console.log(cachedSquare(2)); // square should not be invoked but the same result returned from the cache
console.log(cachedSquare(3)); // square should be invoked
console.log(cachedSquare(3)); 
