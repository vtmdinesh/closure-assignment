const counterFactoryFunction = ()=> {
    let count = 0;
    return {
        increment :()=> {return count +=1} ,
        decrement :()=> {return count -=1}
    }
}

module.exports = counterFactoryFunction