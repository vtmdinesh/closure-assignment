const cacheFunction = (cb) => {
    let cacheObject = {}
    let stringifiedKey = ""

    function testFunction(...argArray) {

        stringifiedKey = argArray.toString()

        if (stringifiedKey in cacheObject) {
            //console.log("Inside cache")
            return (cacheObject[stringifiedKey])
        }
        else {
            //console.log("Invoking cb")
            cacheObject[stringifiedKey] = cb(...argArray)
            return cacheObject[stringifiedKey]
        }
    }
    return testFunction
}

module.exports = cacheFunction

